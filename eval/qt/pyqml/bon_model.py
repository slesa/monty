from PySide2.QtCore import QAbstractItemModel, QModelIndex, Qt


class BonItemModel(QAbstractItemModel):

    _tableCol = Qt.UserRole + 1
    _partyCol = Qt.UserRole + 2
    _waiterCol = Qt.UserRole + 3
    _waterNameCol = Qt.UserRole + 4
    _orderCountCol = Qt.UserRole + 5

    def __init__(self, data=None):
        QAbstractItemModel.__init__(self, None)
        self.bons = data
        self.row_count = len(data)

    def index(self, row, column, parent=QModelIndex, **kwargs):
        return self.createIndex(row, column, parent)

    def rowCount(self, parent=QModelIndex):
        return self.row_count

    def data(self, index, role=Qt.DisplayRole):
        row = index.row()
        if index.isValid() and 0 <= row < self.rowCount():
            bon = self.bons[index.row()]
            if role == BonItemModel._tableCol:
                return bon.table
            if role == BonItemModel._partyCol:
                return bon.party
            if role == BonItemModel._waiterCol:
                return bon.waiter
            if role == BonItemModel._waterNameCol:
                return bon.waiterName
            if role == BonItemModel._orderCountCol:
                return len(bon.orders)
        return None

    def roleNames(self):
        return {
            BonItemModel._tableCol: b'table',
            BonItemModel._partyCol: b'party',
            BonItemModel._waiterCol: b'waiter',
            BonItemModel._waterNameCol: b'waiterName',
            BonItemModel._orderCountCol: b'orderCount'
        }
