import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12

Page {
    width: 640
    height: 480

    Material.theme: Material.Dark
    Material.accent: Material.Purple

    header: Label {
        //color: "#15af15"
        text: qsTr("Data Viewer")
        //font.pointSize: 17
        //font.bold: true
        //font.family: "Arial"
        renderType: Text.NativeRendering
        horizontalAlignment: Text.AlignHCenter
        padding: 10
    }
    Rectangle {
        id: root
        width: parent.width
        height: parent.height
        color: Material.background

        /* Image {
            id: image
            fillMode: Image.PreserveAspectFit
            anchors.centerIn: root
            source: "./logo.png"
            opacity: 0.5

        } */

        Component {
            id: bonDelegate

            Row {
                spacing: 3
                anchors.left: parent.left
                anchors.leftMargin: 2
                anchors.right: parent.right

                Text { text: table; color: Material.foreground }
                Text { text: party; color: Material.foreground }
                Text { text: waiter; color: Material.foreground }
                Text { text: waiterName; color: Material.foreground }
                Text { text: '('+orderCount+')'; color: Material.foreground }
                // font.pixelSize: 24

            }
        }
        ListView {
            id: view
            anchors.fill: root
            anchors.margins: 25
            model: myModel
            delegate: bonDelegate
            focus: true
            highlight: Rectangle {
                color: Material.accent
                width: parent.width
            }
            section {
                property: "waiterName"
                delegate: Rectangle {
                    color: Material.primary
                    width: parent.width
                    height: childrenRect.height + 4
                    Text { anchors.horizontalCenter: parent.horizontalCenter
                        font.pixelSize: 16
                        font.bold: true
                        text: section
                    }
                }
            }
            Component.onCompleted : {
               console.log(myModel)
            }
        }
    }
    NumberAnimation {
        id: anim
        running: true
        target: view
        property: "contentY"
        duration: 500
    }
}
