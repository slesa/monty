# This Python file uses the following encoding: utf-8
import os, sys, urllib.request, json
from typing import Any

import PySide2.QtQml
import jsonpickle
from PySide2.QtQuick import QQuickView
from PySide2.QtCore import QStringListModel, Qt, QUrl
from PySide2.QtGui import QGuiApplication

from bon import Bon
from bon_model import BonItemModel


def dict_to_class(class_name: Any, dictionary: dict) -> Any:
    instance = class_name()
    for key in dictionary.keys():
        setattr(instance, key, dictionary[key])
    return instance


def json_to_class(class_name: Any, json_string: str) -> Any:
    dict_object = json.loads(json_string)
    return dict_to_class(class_name, dict_object)


if __name__ == "__main__":
    url = "http://localhost:8282/bm/bons"
    response = urllib.request.urlopen(url)
    plainjson = response.read().decode('utf-8')
    data = json.loads(plainjson)
    #data = jsonpickle.decode(plainjson)
    ticks = data['requestedAtTicks']
    jsonbons = data['bons']
    bons = []
    for jsonbon in jsonbons:
        bon = Bon()
        for key,value in jsonbon.items():
            bon.__setattr__(key, value)
        bons.append(bon)

#    data_list = bons  # jsonpickle.decode(bons)
#    data_list.sort()

    app = QGuiApplication(sys.argv)
    #app = QGuiApplication(['-qmljsdebugger=port:8288,block'])
    #app = QGuiApplication(['-help'])
    view = QQuickView()
    view.setResizeMode(QQuickView.SizeRootObjectToView)

    my_model = BonItemModel(bons) # QStringListModel()
    # my_model.setStringList(data_list)
    view.rootContext().setContextProperty("myModel", my_model)

    qml_file = os.path.join(os.path.dirname(__file__), "view.qml")
    view.setSource(QUrl.fromLocalFile(os.path.abspath(qml_file)))

    if view.status() == QQuickView.Error:
        sys.exit(-1)
    view.show()

    sys.exit(app.exec_())
