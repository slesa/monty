numpy==1.18.2
pandas==1.0.3
PySide2==5.14.2
python-dateutil==2.8.1
pytz==2019.3
shiboken2==5.14.2
six==1.14.0
