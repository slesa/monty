import QtQuick 2.6
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.13

Rectangle {
    id: root
    color: 'black'
    height: 300
    property var currentItem: todoView.currentItem
    property var currentIndex: todoView.currentIndex

    Component {
        id: todoDelegate
        Item {
            id: wrapper
            width: 300
            height: 20
            Row {
                Rectangle {
                    id: check
                    color: done
                    width: 20
                    height: 20
                }
                Text {
                    x: wrapper.x + check.width + 5
                    // width: 100 // wrapper.width - check.width - wrapper.x
                    height: 20
                    text: index + " " + message
                    color: 'white'
                }
            }
            states: State {
                name: "Current"
                when: wrapper.ListView.isCurrentItem
                PropertyChanges { target: wrapper; x: 20 }
            }
            transitions: Transition {
                NumberAnimation { properties: "x"; duration: 200 }
            }
            MouseArea {
                anchors.fill: parent
                onClicked: wrapper.ListView.view.currentIndex = index
            }
        }
    }

    Component {
        id: highlightBar
        Rectangle {
            width: root.width
            height: 20
            color: "#888844"
            y: todoView.currentItem.y
            Behavior on y { SpringAnimation { spring: 2; damping: 0.1 }}
        }
    }

    ListView {
        id: todoView
        model: TodoEntries
        delegate: todoDelegate
        focus: true
        highlight: highlightBar
        highlightFollowsCurrentItem: false
        anchors.fill: parent
        KeyNavigation.tab: deleteButton
    }

}
