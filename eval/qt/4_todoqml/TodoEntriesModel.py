import json

from PyQt5 import QtCore, QtGui
from PyQt5.QtCore import Qt, QAbstractListModel, pyqtSlot, QModelIndex, pyqtSignal


class TodoEntriesModel(QAbstractListModel):

    MessageRole = Qt.UserRole + 1
    DoneRole = Qt.UserRole + 2

    todoChanged = pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent)
        self.todos = [{'message': 'Init repo', 'done': False}]

    def load(self):
        try:
            with open('data.json', 'r') as f:
                self.todos = json.load(f)
        except Exception:
            pass

    def save(self):
        with open('data.json', 'w') as f:
            data = json.dump(self.todos, f)

    @pyqtSlot(str)
    def add(self, text):
        self.beginInsertRows(QModelIndex(), self.rowCount(), self.rowCount())
        self.todos.append({'message': text, 'done': False})
        self.endInsertRows()
        self.save()

    @pyqtSlot(int)
    def remove(self, row):
        # row = index.row()
        self.beginRemoveRows(QModelIndex(), row, row)
        self.todos.pop(row)
        self.endRemoveRows()
        self.save()

    @pyqtSlot(int)
    def complete(self, row):
        # row = index.row()
        self.beginResetModel()
        message, done = self.todos[row]
        self.todos[row] = {'message': message, 'done': True}
        self.endResetModel()
        self.save()

    def data(self, index, role):
        row = index.row()
        if role == TodoEntriesModel.MessageRole:
            return self.todos[row]["message"]
        if role == TodoEntriesModel.DoneRole:
            done = self.todos[row]["done"]
            if done:
                return QtGui.QColor('green')
            else:
                return QtGui.QColor('yellow')

    def rowCount(self, parent=QModelIndex()):
        return len(self.todos)

    def roleNames(self):
        return {
            TodoEntriesModel.MessageRole: b'message',
            TodoEntriesModel.DoneRole: b'done'
        }
