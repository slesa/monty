import QtQuick 1.0

ListModel {
    ListElement { message: 'Init git repo'; done: false }
    ListElement { message: 'Include build system'; done: false }
    ListElement { message: 'Move old repos to new one'; done: true }
}
