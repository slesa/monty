import sys

from PyQt5.QtCore import QUrl
from PyQt5.QtQml import QQmlApplicationEngine, qmlRegisterType, QQmlComponent
from PyQt5.QtQuick import QQuickView
from PyQt5.QtWidgets import QApplication

from TodoEntriesModel import TodoEntriesModel


app = QApplication(sys.argv)

engine = QQmlApplicationEngine()

model = TodoEntriesModel()
model.load()

ctx = engine.rootContext()
ctx.setContextProperty('TodoEntries', model)
engine.load('main.qml')
win = engine.rootObjects()[0]

win.show()
sys.exit(app.exec_())
