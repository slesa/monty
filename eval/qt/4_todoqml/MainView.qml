import QtQuick 2.0
import QtQuick.Controls 2.6

Column {

    spacing: 3
    // width: parent.width
    // height: parent.height

    TodoEntriesView {
        id: todoView
        height: parent.height - 80
        width: parent.width-20
        //x: 100
        //y: 50
    }

    Rectangle {
        height: 30
        width: parent.width-20
        color: 'black'
        Button {
            id: deleteButton
            text: qsTr("Delete")
            enabled: todoView.currentItem!=null
            KeyNavigation.tab: completeButton
            anchors {
                left: parent.left
                top: parent.top
                bottom: parent.bottom
            }
            onClicked: {
                console.debug("Removing index ", todoView.currentIndex)
                TodoEntries.remove(todoView.currentIndex)
                console.debug("Todo entry count ", TodoEntries.rowCount())
            }
        }
        Button {
            id: completeButton
            text: qsTr("Complete")
            enabled: todoView.currentItem!=null // && todoView.currentItem.done===false
            KeyNavigation.tab: todoEdit
            anchors {
                right: parent.right
                top: parent.top
                bottom: parent.bottom
            }
            onClicked: {
                TodoEntries.complete(todoView.currentIndex)
            }
        }
    }

    Rectangle {
        color: 'blue'
        height: 20
        width: parent.width-20
        TextInput {
            id: todoEdit
            color: 'white'
            KeyNavigation.tab: addButton
            anchors.fill: parent
        }
    }
    Button {
        id: addButton
        text: qsTr("Add todo")
        height: 30
        width: parent.width-20
        enabled: todoEdit.text.length>0
        onClicked: {
            TodoEntries.add(todoEdit.text)
            todoEdit.text = ''
            console.debug("Todo entry count ", TodoEntries.rowCount())
        }
        KeyNavigation.tab: todoView
    }
}
