import QtQuick 2.0
import QtQuick.Controls 1.4

ApplicationWindow {
    id: root
    visible: true
    title: qsTr("Todo QML")
    width: 640
    height: 480

    Image {
        id: background
        source: 'res/background.jpg'
        anchors.fill: parent
        Behavior on rotation {
            NumberAnimation {
                duration: 250
            }
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: background.rotation += 90
    }

    MainView {
        anchors {
            right: parent.right
            verticalCenter: parent.verticalCenter
        }
        width: root.width*0.8
        height: root.height*0.8
    }
}