from PyQt5 import QtCore, QtGui
from PyQt5.QtCore import Qt


class TodoModel(QtCore.QAbstractListModel):
    def __init__(self, *args, todos=None, **kwargs):
        super(TodoModel, self).__init__(*args, **kwargs)
        self.todos = todos or []

    def data(self, index, role):
        if role == Qt.DisplayRole:
            _, text = self.todos[index.row()]
            return text
        if role == Qt.DecorationRole:
            status, _ = self.todos[index.row()]
            if status:
                return QtGui.QColor('green')
            else:
                return QtGui.QColor('yellow')

    def rowCount(self, index):
        return len(self.todos)
