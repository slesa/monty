from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Text

Base = declarative_base()

# https://www.pythoncentral.io/introductory-tutorial-python-sqlalchemy/

class Message(Base):
    __tablename__ = 'messages'

    id = Column(Integer, primary_key=True)
    message = Column(Text)


if __name__ == "__main__":
    from sqlalchemy import create_engine
    from sqlalchemy.orm import sessionmaker

    engine = create_engine('sqlite://')

    message = Message(message='Hello, world')

    Session = sessionmaker(bind=engine)
    session = Session()

    session.add(message)
    session.commit()

    query = session.query(Message)
    instance = query.first()
    print (instance.message)
