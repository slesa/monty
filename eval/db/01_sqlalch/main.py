from sqlalchemy import create_engine, Column, Integer, Text, MetaData, Table, select

engine = create_engine('sqlite://')

metaData = MetaData()
messages = Table(
    'messages', metaData,
    Column('id', Integer, primary_key=True),
    Column('message', Text)
)

messages.create(bind=engine)

insert_message = messages.insert().values(message='Hello, world!')
engine.execute(insert_message)

stmt = select([messages.c.message])
message, = engine.execute(stmt).fetchone()
print(message)

