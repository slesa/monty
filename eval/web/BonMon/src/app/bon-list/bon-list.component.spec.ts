import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BonListComponent } from './bon-list.component';

describe('BonListComponent', () => {
  let component: BonListComponent;
  let fixture: ComponentFixture<BonListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BonListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BonListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
