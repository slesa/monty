import { Component, OnInit } from '@angular/core';
import { Bon } from '../shared/bon';
import { BonStoreService } from '../shared/bon-store.service';

@Component({
  selector: 'app-bon-list',
  templateUrl: './bon-list.component.html',
  styleUrls: ['./bon-list.component.css']
})
export class BonListComponent implements OnInit {
  bons: Bon[];

  constructor(private bs: BonStoreService) {
  }

  ngOnInit(): void {
    this.bons = this.bs.getAll();
  }

}
