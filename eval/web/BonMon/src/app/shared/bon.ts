import { BonOrder } from './bon-order';

export interface Bon {
    id: number;
    isArchived: boolean;
    importedAtToTicks: number;
    table: number;
    party: number;
    tableGuid: string;
//    creationDateTime: Date
    waiter: number;
    waiterName: string;

    orders: BonOrder[];
}
