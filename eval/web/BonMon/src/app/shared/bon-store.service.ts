import { Injectable } from '@angular/core';
import { Bon } from './bon';

@Injectable({
  providedIn: 'root'
})
export class BonStoreService {
  bons: Bon[];

  constructor() {
    this.bons = [
      {
        id: 1,
        isArchived: true,
        importedAtToTicks: 123456789,
        table: 101,
        party: 1,
        tableGuid: 'table-guid',
        waiter: 1,
        waiterName: 'Captain Jean Luc Picard',
        orders: [
          {
            id: 1,
            isArchived: false,
            bonId: 5001,
            entryGuid: '4711-9918',
            plu: 2203,
            article: 'Tomatensaft 0.2',
            count: 4,
          }
        ]
      },
      {
        id: 2,
        isArchived: false,
        importedAtToTicks: 987654321,
        table: 201,
        party: 1,
        tableGuid: 'table-guidance',
        waiter: 2,
        waiterName: 'Captain Cathrin Janeway',
        orders: [
          {
            id: 2,
            isArchived: false,
            bonId: 5001,
            entryGuid: '4711-9918',
            plu: 2309,
            article: 'Cola 0.2',
            count: 4,
          },
          {
            id: 3,
            isArchived: false,
            bonId: 5001,
            entryGuid: '4711-9918',
            plu: 2308,
            article: 'Fanta 0.2',
            count: 2,
          }
        ]
      }
    ];
   }

   getAll(): Bon[] {
     return this.bons;
   }
}
