export interface BonOrder {
    id: number;
    isArchived: boolean;
    bonId: number;
    entryGuid: string;
    plu: number;
    article: string;
    count: number;
}
