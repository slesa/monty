import { TestBed } from '@angular/core/testing';

import { BonStoreService } from './bon-store.service';

describe('BonStoreService', () => {
  let service: BonStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BonStoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
