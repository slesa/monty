import time
from watchdog.observers import Observer
from watchdog.events import PatternMatchingEventHandler


class FileWatcher(object):

    def __init__(self, directory):
        self.directory = directory
        self.observer = Observer()

    def start(self):
        ignore_directories = True
        case_sensitive = False
        event_handler = PatternMatchingEventHandler("*.xml", "", ignore_directories, case_sensitive)
        event_handler.on_created = self.on_file_created
        event_handler.on_modified = self.on_file_modified
        self.observer.schedule(event_handler, self.directory, recursive=False)
        self.observer.start()

    def stop(self):
        self.observer.stop()
        self.observer.join()

    @staticmethod
    def on_file_created(event):
        print("Received created event - %s" % event.src_path)

    @staticmethod
    def on_file_modified(event):
        print("Received modified event - %s" % event.src_path)


if __name__ == "__main__":
    import os

    watchPath = "tmp"
    if not os.path.exists(watchPath):
        os.makedirs(watchPath)

    fw = FileWatcher(watchPath)
    fw.start()
    input("Press Enter to stop")
    fw.stop()
