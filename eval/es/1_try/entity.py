import uuid

from eventsourcing.infrastructure.eventsourcedrepository import EventSourcedRepository
from eventsourcing.infrastructure.eventstore import EventStore

from database import record_manager, sequenced_item_mapper
from domainmodel import Created, Discarded, AttributeChanged
from infrastructure import publish, subscribe


class Example(object):

    def __init__(self, originator_id, originator_version=0, foo=''):
        self._id = originator_id
        self.__version__ = originator_version
        self._is_discarded = False
        self._foo = foo

    @property
    def id(self):
        return self._id

    #@property
    #def __version__(self):
    #    return self.__version__

    @property
    def foo(self):
        return self._foo

    @foo.setter
    def foo(self, value):
        assert not self._is_discarded

        event = AttributeChanged(originator_id=self.id, originator_version=self.__version__, name='foo', value=value)
        mutate(self, event)
        publish(event)

    def discard(self):
        assert not self._is_discarded

        event = Discarded(originator_id=self.id, originator_version=self.__version__)
        mutate(self, event)
        publish(event)


def create_new_example(foo):
    entity_id = uuid.uuid4()

    event = Created(originator_id=entity_id, foo=foo)

    entity = mutate(None, event)

    publish(event=event)
    return entity


def mutate(entity, event):
    if isinstance(event, Created):
        entity = Example(**event.__dict__)
        entity.__version__ += 1
        return entity

    elif isinstance(event, AttributeChanged):
        assert not entity._is_discarded
        setattr(entity, '_' + event.name, event.value)
        entity.__version__ += 1
        return entity

    elif isinstance(event, Discarded):
        assert not entity._is_discarded
        entity.__version__ += 1
        entity._is_discarded = True
        return None
    else:
        raise NotImplementedError(type(event))


event_store = EventStore(record_manager=record_manager, sequenced_item_mapper=sequenced_item_mapper)
example_repository = EventSourcedRepository(event_store=event_store, mutator_func=mutate)


if __name__ == "__main__":

    received_events = []
    subscribe(lambda e: received_events.append(e))

    entity = create_new_example(foo='bar')

    assert entity.id
    assert entity.__version__ == 1

    assert len(received_events)==1, received_events
    assert isinstance(received_events[0], Created)
    assert received_events[0].originator_id == entity.id
    assert received_events[0].originator_version == 0
    assert received_events[0].foo == 'bar'

    assert entity.foo == 'bar'

    entity.foo = 'baz'

    assert entity.foo == 'baz'
    assert entity.__version__ == 2

    assert len(received_events) == 2, received_events
    assert isinstance(received_events[1], AttributeChanged)
    assert received_events[1].originator_id == entity.id
    assert received_events[1].originator_version == 1
    assert received_events[1].name == 'foo'
    assert received_events[1].value == 'baz'

    for event in received_events:
        event_store.store(event)

    stored_events = event_store.get_domain_events(entity.id)
    assert len(stored_events) == 2, (received_events, stored_events)

    retrieved_entity = example_repository[entity.id]
    assert retrieved_entity.foo == 'baz'

    sequenced_items = event_store.record_manager.list_items(entity.id)
    assert len(sequenced_items) == 2

    assert sequenced_items[0].sequence_id == entity.id
    assert sequenced_items[0].position == 0
    assert 'Created' in sequenced_items[0].topic
    assert 'bar' in sequenced_items[0].state

    assert sequenced_items[1].sequence_id == entity.id
    assert sequenced_items[1].position == 1
    assert 'AttributeChanged' in sequenced_items[1].topic
    assert 'baz' in sequenced_items[1].state
