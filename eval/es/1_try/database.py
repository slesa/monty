from eventsourcing.infrastructure.eventsourcedrepository import EventSourcedRepository
from eventsourcing.infrastructure.eventstore import EventStore
from eventsourcing.infrastructure.sequenceditemmapper import SequencedItemMapper
from eventsourcing.infrastructure.sqlalchemy.datastore import SQLAlchemyDatastore, SQLAlchemySettings
from eventsourcing.infrastructure.sqlalchemy.manager import SQLAlchemyRecordManager
from sqlalchemy.ext.declarative.api import declarative_base
from sqlalchemy.sql.schema import Column, Sequence, Index
from sqlalchemy.sql.sqltypes import BigInteger, Integer, String, Text
from sqlalchemy_utils import UUIDType

Base = declarative_base()


class IntegerSequenceRecord(Base):
    __tablename__ = 'integer_sequenced_items'

    id = Column(BigInteger().with_variant(Integer, "sqlite"), primary_key=True)
    sequence_id = Column(UUIDType(), nullable=False)
    position = Column(BigInteger(), nullable=False)
    topic = Column(String(255))
    state = Column(Text())

    __table_args__ = Index('index', 'sequence_id', 'position', unique=True),


datastore = SQLAlchemyDatastore(base=Base, settings=SQLAlchemySettings(uri='sqlite:///:memory:'))
datastore.setup_connection()
datastore.setup_table(IntegerSequenceRecord)


record_manager = SQLAlchemyRecordManager(session=datastore.session, record_class=IntegerSequenceRecord)
sequenced_item_mapper = SequencedItemMapper(sequence_id_attr_name='originator_id', position_attr_name='originator_version')

