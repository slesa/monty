subscribers = []


def publish(event):
    for subscriber in subscribers:
        subscriber(event)


def subscribe(subscriber):
    subscribers.append(subscriber)


def unsubscribe(subscriber):
    subscribers.remove(subscriber)

