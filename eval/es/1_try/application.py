from eventsourcing.infrastructure.eventsourcedrepository import EventSourcedRepository
from eventsourcing.infrastructure.eventstore import EventStore
from eventsourcing.infrastructure.sequenceditemmapper import SequencedItemMapper
from eventsourcing.infrastructure.sqlalchemy.manager import SQLAlchemyRecordManager
from eventsourcing.infrastructure.sqlalchemy.records import IntegerSequencedRecord

from database import datastore
from entity import mutate, create_new_example
from infrastructure import unsubscribe, subscribe


class PersistencePolicy(object):
    def __init__(self, event_store):
        self.event_store = event_store
        subscribe(self.store_event)

    def close(self):
        unsubscribe(self.store_event)

    def store_event(self, event):
        self.event_store.store(event)


class ExampleApplication(object):
    def __init__(self, session):
        self.event_store = EventStore(
            record_manager=SQLAlchemyRecordManager(
                record_class=IntegerSequencedRecord,
                session=session
            ),
            sequenced_item_mapper=SequencedItemMapper(
                sequence_id_attr_name='originator_id',
                position_attr_name='originator_version'
            ))
        self.persistence_policy = PersistencePolicy(event_store=self.event_store)
        self.example_repository = EventSourcedRepository(event_store=self.event_store, mutator_func=mutate)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.persistence_policy.close()


if __name__=="__main__":
    with ExampleApplication(datastore.session) as app:
        example = create_new_example(foo='bar')

        assert example.id in app.example_repository
        assert app.example_repository[example.id].foo == 'bar'

        example.foo = 'baz'
        assert app.example_repository[example.id].foo == 'baz'

        example.discard()
        assert example.id not in app.example_repository
