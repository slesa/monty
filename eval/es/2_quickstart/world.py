from eventsourcing.domain.model.aggregate import AggregateRoot
from eventsourcing.domain.model.decorators import attribute


class World(AggregateRoot):
    def __init__(self, ruler=None, **kwargs):
        super(World, self).__init__(**kwargs)
        self._history = []
        self._ruler = ruler

    @property
    def history(self):
        return tuple(self._history)

    @attribute
    def ruler(self):
        """ A mutable event-sourced attribute """

    def make_it_so(self, something):
        self.__trigger_event__(World.SomethingHappened, what=something)

    class SomethingHappened(AggregateRoot.Event):
        def mutate(self, obj):
            obj._history.append(self.what)


if __name__ == "__main__":
    world = World.__create__(ruler='gods')
    assert world.ruler == 'gods'

    world.make_it_so('dinosaurs')
    world.make_it_so('trucks')

    world.ruler = 'money'

    assert world.history == ('dinosaurs', 'trucks'), world.history
    assert world.ruler == 'money'
