from eventsourcing.application.sqlalchemy import SQLAlchemyApplication
from eventsourcing.exceptions import ConcurrencyError

from world import World

if __name__ == "__main__":
    with SQLAlchemyApplication(persist_event_type=World.Event) as app:
        world = World.__create__(ruler='gods')

        world.make_it_so('dinosaurs')
        world.make_it_so('trucks')

        version = world.__version__
        world.make_it_so('internet')

        world.ruler = 'money'

        assert world.ruler == 'money'
        assert world.history[2] == 'internet'
        assert world.history[1] == 'trucks'
        assert world.history[0] == 'dinosaurs'

        world.__save__()

        copy = app.repository[world.id]
        assert isinstance(copy, World)

        assert copy.ruler == 'money'
        assert world.history[2] == 'internet'
        assert world.history[1] == 'trucks'
        assert world.history[0] == 'dinosaurs'

        assert copy.__head__ == world.__head__

        world.__discard__()
        world.__save__()

        assert world.id not in app.repository
        try:
            app.repository[world.id]
        except KeyError:
            pass
        else:
            raise Exception("Should't get here")

        old = app.repository.get_entity(world.id, at=version)
        assert old.history[-1] == 'trucks'
        assert len(old.history) == 2
        assert old.ruler == 'gods'

        old.make_it_so('future')
        try:
            old.__save__()
        except ConcurrencyError:
            pass
        else:
            raise Exception("Shouldn't get here")

        events = app.event_store.get_domain_events(world.id)
        last_hash = ''
        for event in events:
            event.__check_hash__()
            assert event.__previous_hash__ == last_hash
            last_hash = event.__event_hash__

        assert last_hash == world.__head__

        from eventsourcing.application.notificationlog import NotificationLogReader
        reader = NotificationLogReader(app.notification_log)
        notifications = reader.read()
        notifications_ids = [n['id'] for n in notifications]
        assert notifications_ids == [1, 2, 3, 4, 5, 6]

        record_manager = app.event_store.record_manager
        items = record_manager.get_items(world.id)
        for item in items:
            assert item.originator_id == world.id
            assert 'dinosaurs' not in item.state
            assert 'trucks' not in item.state
            assert 'internet' not in item.state
