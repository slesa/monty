class NotFound(Exception):
    pass


class ConcurrentStreamWriteError(Exception):
    pass