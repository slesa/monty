import json
import os
from table import TableId, PartyId, Table


class InvalidTableId(Exception):
    pass


class InvalidPartyId(Exception):
    pass


class TableFile:
    @property
    def table_path(self):
        return os.path.join("var", "tables")

    def get_table_file_name(self, table_id: TableId, party_id: PartyId = 0):
        if table_id <= 0:
            raise InvalidTableId();
        if party_id < 0:
            raise InvalidPartyId()

        file_name = "T%d" % table_id
        if party_id > 0:
            file_name += ".%d" % party_id

        return os.path.join(self.table_path, file_name)


class TableLoader(TableFile):
    """ Tries to open a table file, creates a new table if no file was found """

    def __init__(self):
        super(TableLoader, self)

    def load(self, waiter_id, table_id: TableId, party_id: PartyId = 0):
        fn = self.get_table_file_name(table_id, party_id)
        try:
            with open(fn, 'r') as fh:
                actions = json.load(fh)
                return Table(table_id, party_id, actions)
        except Exception:
            return Table.create(waiter_id=waiter_id, table_id=table_id, party_id=party_id)

