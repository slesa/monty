from dataclasses import dataclass


@dataclass
class OrderCreated:
    user_id: int
    """
    def __init__(self, user_id):
        self.user_id = user_id

    def __eq__(self, other):
        if isinstance(self, other.__class__):
            return self.__dict__ == other.__dict__
    """


@dataclass
class StatusChanged:
    new_status: str
    """
    def __init__(self, new_status):
        self.new_status = new_status

    def __eq__(self, other):
        if isinstance(self, other.__class__):
            return self.__dict__ == other.__dict__
    """


