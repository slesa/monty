from database import EventStream
from dispatch import method_dispatch
from events import OrderCreated, StatusChanged


class Order:
    def __init__(self, event_stream: EventStream):
        self.version = event_stream.version

        for event in event_stream.events:
            self.apply(event)
        self.changes = []

    @classmethod
    def create(cls, user_id):
        initial_event = OrderCreated(user_id)
        instance = cls([initial_event])
        instance.changes = [initial_event]
        return instance

    @method_dispatch
    def apply(self, event):
        raise ValueError('Unknown event!')

    @apply.register(OrderCreated)
    def _(self, event: OrderCreated):
        self.user_id = event.user_id
        self.status = 'new'

    @apply.register(StatusChanged)
    def _(self, event: StatusChanged):
        self.status = event.new_status

    def set_status(self, new_status):
        if new_status not in ('new', 'paid', 'confirmed'):
            raise ValueError(f'{new_status} is not a correct status')
        event = StatusChanged(new_status)
        self.apply(event)
        self.changes.append(event)





