class TableEvent:
    pass


class TableCreated(TableEvent):
    def __init__(self, waiter_id):
        self.waiter_id = waiter_id
