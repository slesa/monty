import abc
import json
import typing
import uuid

from sqlalchemy import VARCHAR, Column, Integer, ForeignKey, JSON, exc
from sqlalchemy.orm import relationship, Session, joinedload
from sqlalchemy.orm.attributes import Event

import events
from errors import NotFound, ConcurrentStreamWriteError


class EventStream:
    events: typing.List[Event]
    version: int

    def __init__(self, events, version):
        pass


class EventStore(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def load_stream(self, aggregate_uuid: uuid.UUID) -> EventStream:
        pass

    @abc.abstractmethod
    def append_to_stream(self,
                         aggregate_uuid: uuid.UUID,
                         expected_version: typing.Optional[int],
                         events: typing.List[Event]) -> None:
        pass


class AggregateModel(object):
    __tablename__ = 'aggregates'

    uuid = Column(VARCHAR(36), primary_key=True)
    version = Column(Integer, default=1)


class EventModel(object):
    __tablename__ = 'events'

    uuid = Column(VARCHAR(36), primary_key=True)
    aggregate_uuid = Column(VARCHAR(36), ForeignKey('aggregates.uuid'))
    name = Column(VARCHAR(50))
    data = Column(JSON)

    aggregate = relationship(AggregateModel, uselist=False, backref='events')


class PostgreSQLEventStore(EventStore):

    def __init__(self, session: Session):
        self.session = session

    def load_stream(self, aggregate_uuid: uuid.UUID):
        try:
            aggregate: AggregateModel = self.session.query(AggregateModel).\
                options(joinedload('events')).\
                filter(AggregateModel.uuid == str(aggregate_uuid)).\
                one()
        except exc.NoResultFound:
            raise NotFound

        events_objects = [self._translate_to_object(model) for model in aggregate.events]
        version = aggregate.version

        return EventStream(events_objects, version)

    def _translate_to_object(self, event_model: EventModel):
        class_name = event_model.name
        kwargs = event_model.data
        event_class: typing.Type[Event] = getattr(events, class_name)
        return event_class(**kwargs)


    def append_to_stream(self,
                         aggregate_uuid: uuid.UUID,
                         expected_version: typing.Optional[int],
                         events: typing.List[Event]):
        connection = self.session.connection()

        if expected_version:
            stmt = AggregateModel.__table__.update().values(
                version=expected_version+1
            ).where(
                (AggregateModel.version == expected_version) &
                (AggregateModel.uui == str(aggregate_uuid))
            )
            result_proxy = connection.execute(stmt)

            if result_proxy.rowcount != 1:
                raise ConcurrentStreamWriteError
        else:
            stmt = AggregateModel.__table__.insert().values(
                uuid = str(aggregate_uuid),
                version = 1
            )
            connection.execute(stmt)

        for event in events:
            aggregate_uuid_str = str(aggregate_uuid),
            event_as_dict = event.as_dict()

            connection.execute(
                EventModel.__table__.insert().values(
                    uuid = str(uuid.uuid4()),
                    aggregate_uuid = aggregate_uuid_str,
                    name = event.__class__.__name__,
                    data = event_as_dict
                )
            )

            payload = json.dumps(event_as_dict)
            connection.execute(f'NOTIFY events, \'{aggregate_uuid_str}_{event.__class__.__name__}_{payload}\'')