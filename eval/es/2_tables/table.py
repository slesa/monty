from typing import List

from table_events import TableCreated

TableId = int
PartyId = int


class Table:

    def __init__(self, table_id: TableId, party_id: PartyId, actions: List = []):
        self.table_id = table_id
        self.party_id = party_id
        self.actions = actions

    @classmethod
    def create(cls, waiter_id, table_id, party_id):
        initial_event = TableCreated(waiter_id)
        instance = cls(table_id, party_id, [initial_event])
        instance.changes = [initial_event]
        return instance

