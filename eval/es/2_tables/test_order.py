from unittest import TestCase

from events import OrderCreated, StatusChanged
from order import Order


class OrderAggregateTest(TestCase):
    def test_should_create_order(self):
        order = Order.create(user_id=1)
        self.assertEqual(order.changes, [OrderCreated(user_id=1)])

    def test_should_emit_set_status_event(self):
        order = Order([OrderCreated(user_id=1)])
        order.set_status('confirmed')
        self.assertEqual(order.changes, [StatusChanged('confirmed')])

