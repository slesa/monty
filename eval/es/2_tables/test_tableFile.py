import unittest
from unittest import TestCase

from table_file import TableFile, InvalidTableId, InvalidPartyId, TableLoader


class TestTableFile(TestCase):
    def setUp(self):
        pass

    def test_get_table_file_name_with_zero_table_id(self):
        sut = TableFile()
        self.assertRaises(InvalidTableId, lambda: sut.get_table_file_name(0))
                          # "zero table id results in an InvalidTableId exception"

    def test_get_table_file_name_with_negative_table_id(self):
        sut = TableFile()
        self.assertRaises(InvalidTableId, lambda: sut.get_table_file_name(-1))
        # "negative table id results in an InvalidTableId exception")

    def test_get_table_file_name_with_negative_party_id(self):
        sut = TableFile()
        self.assertRaises(InvalidPartyId, lambda: sut.get_table_file_name(1, -1))
        # "negative party id results in an InvalidTableId exception")

    def test_get_table_file_name_with_party_id_zero(self):
        sut = TableFile()
        fn = sut.get_table_file_name(1)
        self.assertEqual(fn, "var/tables/T1")

    def test_get_table_file_name_with_table_and_party_id_set(self):
        sut = TableFile()
        fn = sut.get_table_file_name(42,1)
        self.assertEqual(fn, "var/tables/T42.1")


class TestTableLoader(TestCase):
    def setUp(self):
        self._sut = TableLoader()

    def test_when_loading_nonexisting_table(self):
        table = self._sut.load(waiter_id=1, table_id=1)
        self.assertEqual(1, len(table.actions))

if __name__ == "__main__":
    unittest.main()
