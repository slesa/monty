namespace Scheidt.Core.Domain
{
    public class Sprache
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}