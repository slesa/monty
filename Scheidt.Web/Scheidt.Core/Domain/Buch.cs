namespace Scheidt.Core.Domain
{
    public class Buch
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Sprache Sprache { get; set; }
    }
}