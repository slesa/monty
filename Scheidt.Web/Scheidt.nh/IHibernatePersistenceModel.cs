using FluentNHibernate.Cfg;

namespace Scheidt.nh
{
    public interface IHibernatePersistenceModel
    {
        void AddMappings(MappingConfiguration configuration);
    }
}