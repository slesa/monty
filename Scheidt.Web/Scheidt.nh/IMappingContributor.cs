using FluentNHibernate.Cfg;

namespace Scheidt.nh
{
    public interface IMappingContributor
    {
        void Apply(MappingConfiguration configuration);
    }
}