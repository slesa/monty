using NHibernate;

namespace Scheidt.nh
{
    public interface IDomainQuery<out TResult>
    {
        TResult Execute(ISession session);
    }
}