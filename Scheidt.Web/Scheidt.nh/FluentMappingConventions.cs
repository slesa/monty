using FluentNHibernate.Cfg;
using FluentNHibernate.Conventions.Helpers;

namespace Scheidt.nh
{
    public class FluentMappingConventions : IMappingContributor
    {
        public void Apply(MappingConfiguration configuration)
        {
            var conventions = configuration.FluentMappings.Conventions;
            conventions.Add(ConventionBuilder
                .Class
                .Always(x => x.Table(Inflector.Inflector.Pluralize(x.EntityType.Name).Escape())));
        }
    }
    
    static class StringExtensions
    {
        public static string Escape(this string instance)
        {
            return $"`{instance}`";
        }
    }

}