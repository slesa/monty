using FluentNHibernate.Cfg.Db;

namespace Scheidt.nh
{
    public interface IPersistenceConfiguration
    {
        IPersistenceConfigurer GetConfiguration();
    }
}