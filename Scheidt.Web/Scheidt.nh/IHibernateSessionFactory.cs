using System;
using NHibernate;

namespace Scheidt.nh
{
    public interface IHibernateSessionFactory : IDisposable
    {
        ISession CreateSession();
    }
}