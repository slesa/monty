using NHibernate;

namespace Scheidt.nh
{
    public interface IHibernateInitializationAware
    {
        void Initialized(NHibernate.Cfg.Configuration configuration, ISessionFactory sessionFactory);
    }
}